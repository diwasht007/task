<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WordCountController extends Controller
{
    const SIZE = 26;
    public function getWordCount(Request $request)
    {
        $str = $request->str;
        return self::countWord($str);
    }
    // function to print the character and its frequency in order of its occurrence
    private static function countWord($str)
    {

        //size of the string 'str'
        $n = strlen($str);

        // 'freq[]' implemented as hash table
        $freq = array_fill(0, self::SIZE, NULL);
        $result = [];
        // accumulate frequency of each character in 'str'
        for ($i = 0; $i < $n; $i++)
            $freq[ord($str[$i]) - ord('a')]++;

        // traverse 'str' from left to right  
        for ($i = 0; $i < $n; $i++) {


            if ($freq[ord($str[$i]) - ord('a')] != 0) {
                $result[] =  $str[$i] . ":" . $freq[ord($str[$i]) - ord('a')] . " ";
                $freq[ord($str[$i]) - ord('a')] = 0;
            }
        }
        //array to string
        return implode(",", $result); //API ROUTE
    }
}
//Reference: https://www.geeksforgeeks.org/print-characters-frequencies-order-occurrence/?ref=gcse