<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DailyMotionController extends Controller
{

    public function search(Request $request)
    {

        $user_input = $request->channel;
        $allowedChannel = array('lifestyle', 'auto', 'videogames', 'music', 'news');
        if (preg_match('(' . implode('|', $allowedChannel) . ')', $user_input))
            return $this->dailySearch($request->search, $user_input);
        else {
            return response()->json(['error' => 'please enter channel lifestyle, auto, videogames, music or news']);
        }
    }

    public function dailySearch(string $searchString, string $channelName)
    {
        $response = Http::get('https://api.dailymotion.com/videos', [
            'channel' => $channelName,
            'fields' => 'id,title,owner,owner.screenname,owner.url,channel',
            'search' => $searchString,
            'limit' => 12
        ]);
        $data = json_decode($response->body(), true);
        return response()->json($data); //API ROUTE
    }
}

//Refrence: https://developers.dailymotion.com/api/
