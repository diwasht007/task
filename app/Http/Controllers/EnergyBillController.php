<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EnergyBillController extends Controller
{
    public function index()
    {
        return view('energy_bill.index');
    }
    public function store(Request $request)
    {
        try {
            $units = $request->units;
            $first_fifty_units = 0.20;
            $first_hundred_units = 0.50;
            $second_hundred_units = 0.7;
            $above_twofifty_units = 0.9;

            if ($units <= 50) {
                $bill = $units * $first_fifty_units;
            } elseif ($units > 50 && $units <= 100) {
                $first_fifty_price = 50 * $first_fifty_units;
                $remaining_units = $units - 50;
                $bill = $first_fifty_price + ($remaining_units * $first_hundred_units);
            } elseif ($units > 100 && $units <= 200) {
                $till_first_hundred_price = (50 * $first_fifty_units) + (100 * $first_hundred_units);
                $remaining_units = $units - 150;
                $bill = $till_first_hundred_price + ($remaining_units * $second_hundred_units);
            } else {
                $till_second_hundred_price = (50 * 3.5) + (100 * $first_hundred_units) + (100 * $second_hundred_units);
                $remaining_units = $units - 250;
                $bill = $till_second_hundred_price + ($remaining_units * $above_twofifty_units);
            }
        } catch (Exception $e) {
            Session::flash('error', $e->getMessage());
        }
        return view('energy_bill/index', compact('bill'));
    }
}

//Refrence: https://tutorialsclass.com/exercise/calculate-electricity-bill-in-php/
