<?php

use App\Http\Controllers\DailyMotionController;
use App\Http\Controllers\WordCountController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Exercise 2 Dailymotion search
Route::get('dailymotion',[DailyMotionController::class,'search']);

//Exercise 3 Word Counter
Route::get('counter',[WordCountController::class,'getWordCount']);
